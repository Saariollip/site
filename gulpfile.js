var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.sass('app.scss');

    mix.styles(['vendor/normalize.css',
    	'app.css'//, 'offcanvas.css'
    	], null,'public/css');

   mix.scripts(['app.js', 'offcanvas.js'
   	], null, 'public/js');

     //version control & cache 
    //mix.version(["css/all.css", "js/all.js"]);
});
