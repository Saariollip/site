<!-- posts.index.blade.php -->
@extends('layouts.app')


@section('content')

	    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
        
         <!-- Flash messages -->
         @include('flash::message')

        <!-- New Post Form -->
        <form action="{{ url('/post') }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <!-- Post Name -->
            <div class="form-group">
                <label for="post" class="col-sm-1 control-label">Post</label>

                <div class="col-sm-12">
                    <textarea name="content" id="post-content" class="form-control" cols="5" rows="2" style="resize:none;" placeholder="Lue kirjoitus- ja kommentointisäännöt ennen julkaisua"></textarea>
                </div>
            </div>

            <!-- Add Post Button -->
            <div class="form-group">
                <div class="col-sm-offset-7 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i>New post
                    </button>
                </div>
            </div>
        </form>
    </div>

    <!-- Current Posts -->
    @if (count($posts) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Current Posts
            </div>

            <div class="panel-body">
                <table class="table table-striped Post-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>Post</th>
                        <th>&nbsp;</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($posts as $post)
                            @if($post->category_id = $category->id)
                                <tr>
                                    <!-- Post Name -->
                                    <td class="table-text">
                                        <div>{{ $post->content }}
                                        </div>
                                    </td>
                                    <!-- Delete Button -->
                                    <td>
                                            <form action="{{ url('post/'.$post->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}


                                                <button type="submit" id="delete-post-{{ $post->id }}" class="btn btn-danger" style="float: right;">
    								                <i class="fa fa-btn fa-trash"></i>Delete
    								            </button>
                                            </form>
                                    </td>
                                </tr>
                                @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif


@endsection