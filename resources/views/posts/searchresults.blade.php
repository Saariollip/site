@extends('layouts.app')

@section('content')


	 <table class="table table-bordered table-hover" >

             @if (count($posts) == 0)
                <p> posts not found! </p>
            @else
            <thead>
                <th>Post</th>
            </thead>
            <tbody>
                @foreach($posts as $post)
                <tr>
                    <td>{{ $post->content }}</td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>

@endsection