
	    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
        
         <!-- Flash messages -->
         @include('flash::message')

        <!-- New Post Form -->
        <form action="{{ url('/categories/{id}/post') }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <!-- Post Name -->
            <div class="form-group">
                <label for="post" class="col-sm-1 control-label">Post</label>

                <div class="col-sm-12">
                    <textarea name="content" id="post-content" class="form-control" cols="5" rows="2" style="resize:none;" placeholder="Lue kirjoitus- ja kommentointisäännöt ennen julkaisua"></textarea>
                    

                   
                </div>
            </div>

            
            <!-- Add Post Button -->
            <div class="form-group">
                <div class="col-sm-offset-7 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i>New post
                    </button>
                </div>
            </div>
            
        </form>
    </div>

