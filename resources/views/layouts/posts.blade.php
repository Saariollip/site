    <!-- Current Posts -->
    @if (count($posts) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Current Posts
            </div>

            <div class="panel-body">
                <table class="table table-striped Post-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>Post</th>
                        <th>&nbsp;</th>
                    </thead>
                    

                    <!-- Table Body -->
                    <tbody>
      
                         @foreach ($posts as $post)
                  @if ($post->category_id == $category->id )
                                <tr>
                                    <!-- Post Name -->
                                    <td class="table-text">
                                        <div>{{ $post->content }}
                                        </div>
                                    </td>
                                    <!-- Delete Button -->
                                    <td>
                                            <form action="{{ url('post/'.$post->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}


                                                <button type="submit" id="delete-post-{{ $post->id }}" class="btn btn-danger" style="float: right;">
    								                <i class="fa fa-btn fa-trash"></i>Delete
    								            </button>
                                            </form>
                                    </td>
                                </tr>
                                @endif
                    @endforeach
                    </tbody>
                        
                </table>

            </div>
        </div>
    @endif