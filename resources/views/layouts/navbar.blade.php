 <nav class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a  class="navbar-brand" href="{{ url('/') }}">Site</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li {!! Request::is('tasks') ? ' class="active"' : null !!}><a href="{{ url('/tasks') }}">Home</a></li>
            <li {!! Request::is('posts') ? ' class="active"' : null !!} ><a href="{{ url('/posts') }}">Latest</a></li>
            <li {!! Request::is('categories') ? ' class="active"' : null !!}><a href="{{ url('/categories') }}">Categories</a></li>
          </ul>
            
          @include('layouts.search')


           <ul class="nav navbar-nav navbar-right">

                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li {!! Request::is('login') ? ' class="active"' : null !!}><a href="{{ url('/login') }}">Login</a></li>
                        <li {!! Request::is('register') ? ' class="active"' : null !!}><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                 <li><a href="{{ url('/edit') }}"><i class="fa fa-btn fa-sign-out"></i>Edit profile</a></li>
                                  <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
            </ul>
           </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->

