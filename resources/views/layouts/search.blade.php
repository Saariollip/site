{!! Form::open(['method'=>'GET','url'=>'posts.searchresults','class'=>'navbar-form navbar-left','role'=>'search'])  !!}
<!-- Add button
<a href="{{ url('/create') }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Add</a>
-->
<div class="input-group custom-search-form">
    <input type="text" class="form-control" name="search" placeholder="Search posts">
    <span class="input-group-btn">
        <button class="btn btn-default-sm">
            <i class="fa fa-search"></i>
        </button>
    </span>
</div>
{!! Form::close() !!}