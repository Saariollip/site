
/* App.js */
$('div.alert').not('alert-important').delay(2000).slideUp(2000);

$('div.collapse').delay(200).slideUp(200);

/* offcanvas.js */
$(document).ready(function () {
  $('[data-toggle="offcanvas"]').click(function () {
    $('.row-offcanvas').toggleClass('active')
  });
});
//# sourceMappingURL=all.js.map
