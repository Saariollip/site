<?php
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('welcome');      
    });/*->middleware('guest')->middleware('auth');*/
    /*
    *
    */
    
    //Task routes
    Route::get('/tasks', 'TaskController@index');

    Route::post('/task', 'TaskController@store');

    Route::delete('/task/{task}', 'TaskController@destroy');


    //Post routes
    Route::get('/posts', 'PostController@index');


    Route::get('/posts.searchresults', 'PostController@search');
    //TO:DO
    //Route::get('/post/{post}/edit', 'PostController@update');

    //Route::post('/post', 'PostController@addComment');

    Route::delete('/post/{post}', 'PostController@destroy');


    //Category routes
    Route::get('/categories', 'CategoryController@index');

    Route::get('/categories/{id}', 'CategoryController@show');

    Route::get('/categories/{id}/posts', 'PostController@index');




    Route::auth();
});

   // no middleware (FIX!)
  // Route::post('/post', 'PostController@store');

    Route::post('/categories/{id}/post', 'CategoryController@store');    