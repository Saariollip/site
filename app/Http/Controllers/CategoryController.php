<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class CategoryController extends Controller
{
    //

        /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = \App\Category::all();

        return view('/categories.index', ['categories' => $categories]);
    }

    public function show($id)
    {

       global $globalCategoryId;
       $globalCategoryId = $id;
        echo $globalCategoryId;

        $category = \App\Category::findOrFail($id;
  

        $posts = \App\Post::all();

      
        return view('categories.show', compact('category'), compact('posts'));
    }
    public function store(Request $request, $id)
    {
        
       $this->validate($request, [
        'content' => 'required|max:255',
    ]);

    global $globalCategoryId;
    echo $globalCategoryId;

    $request->user()->posts()->create([
        'content' => $request->content,
        'category_id' => $globalCategoryId
    ]);
    

     //Auth::user()->posts()>create($request->all());


     //Flash message
     flash()->success('Your post has been created succesfully!');

    return back()->with('$category');


    }
}
