<?php

namespace App\Http\Controllers;

/*
use Gate;
use App\User;
*/
use App\Post;
use App\Category;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PostRepository;


class PostController extends Controller
{


	/**
     * The post repository instance.
     *
     * @var PostRepository
     */
    protected $posts;


    /**
     * Create a new controller instance.
     *
     * @param  PostRepository  $posts
     * @return void
     */
      public function __construct(PostRepository $posts)
    {
        $this->middleware('auth');

        $this->posts = $posts;
    }

        /**
	 * Display a list of all of the user's post.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function index(Request $request)
	{
        $categories = \App\Category::all();

	     return view('posts.index', compact($categories), [
            'posts' => $this->posts->forUser($request->user()),
        ]);
	}

	/**
	 * Create a new post.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function store(Request $request, $id)
	{
        
       $this->validate($request, [
        'content' => 'required|max:255',
    ]);


    $category = \App\Category::all()->first();

    
    $request->user()->posts()->create([
        'content' => $request->content,
        'category_id' => $category->id
    ]);
    

     //Auth::user()->posts()>create($request->all());


     //Flash message
     flash()->success('Your post has been created succesfully!');

    return back()->with('$category');

	}

    public function search(){

        $search = \Request::get('search'); 
 
    $posts = \App\Post::where('content','like','%'.$search.'%')
        ->paginate(10);
     

        return view('posts.searchresults', compact('posts'));
    }
    /**
     * Update the given post.
     *
     * @param  int  $id
     * @return Response
     */
    /*
    public function update($id)
    {
        $post = Post::findOrFail($id);

        //using gate
        /*
        if (Gate::denies('update-post', $post)) {
            abort(403);
        }
        */

        //using App\User models authorizable can, cannot methods
        /*
        if ($request->user()->cannot('update-post', $post)) {
            abort(403);
        }
        */

     /*
        $this->authorize('update', $post);

        $post->update();
    }
    
    public function addComment($id)
    {
    	$post = Post::findOrFail($id);
    }
    */
    /**
	 * Destroy the given task.
	 *
	 * @param  Request  $request
	 * @param  Task  $task
	 * @return Response
	 */
	public function destroy(Request $request, Post $post)
	{
        
	    $this->authorize('destroy', $post);

	     $post->delete();

    	return back();
        
	}
}