<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;
use App\Post;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    /*
    public function __construct()
    {
        //
    }
    */
        /**
     * Determine if the given post can be updated by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return bool
     */
        /*
    public function before($user, $ability)
    {
    if ($user->isSuperAdmin()) {
        return true;
    }
    }
    public function update(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }
    */
        public function destroy(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }
    /*
        public function addComment(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }
    */
}
