<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $roles = [
          ['id' => 1, 'power' => 10, 'name' => 'member', 'display_name' => 'Member'],
          ['id' => 2, 'power' => 20, 'name' => 'premium', 'display_name' => 'Club Member'],
          ['id' => 3, 'power' => 30, 'name' => 'moderator', 'display_name' => 'Moderator'],
          ['id' => 4, 'power' => 40, 'name' => 'admin', 'display_name' => 'Administrator'],
          ['id' => 5, 'power' => 50, 'name' => 'superadmin', 'display_name' => 'Super Administrator']
        ];
        // DB::table('roles')->insert($roles);
        // App\Role::create($roles);
        foreach($roles as $role){
            App\Role::create($role);
        }
    }
}
