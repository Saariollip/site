<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories =[
        ['id' => 1, 'name' => 'AdminCafe', 'slug' => 'category-001'],
        ['id' => 2,'name' => 'Heinäkeijo','slug' => 'category-002'],
        ['id' => 3,'name' => 'Funkyla','slug' => 'category-003'],
        ['id' => 4,'name' => 'Jatsisavu','slug' => 'category-004']
        ];
        	
        foreach($categories as $category){
            App\Category::create($category);
        }
    }
}
